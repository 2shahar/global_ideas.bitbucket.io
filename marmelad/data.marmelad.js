module.exports = {
    app      : {
        lang      : 'ru',
        stylus    : {
            theme_color : '#3E50B4'
        },
        GA        : false, // Google Analytics site's ID
        package   : 'ключ перезаписывается значениями из package.json marmelad-сборщика',
        settings  : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        storage   : 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
        buildTime : '',
        controls : [
            'default',
            'brand',
            'success',
            'info',
            'warning',
            'danger'
        ]
    },
    pageTitle: 'marmelad',
    News : [
        {
            img : 'news-img1.png',
            data : '11.11.2018',
            title : 'We work with Talents, StartUps, Investors, Corporations!',
            text : 'Друзья, Мы поддерживаем развитие Талантов во всём Мире! Сейчас Мы первые в Мире покупаем Идеи на любой стадии! Мы ищем тех, кто так же как Мы, может гене рировать идеи постоянно и станет частью нашей Команды!',
            read : 'Read more',
        },
        {
            img : 'news-img2.png',
            data : '11.11.2018',
            title : 'New Industrial Ideas in Construction and Building',
            text : 'Друзья, Мы поддерживаем развитие Талантов во всём Мире! Сейчас Мы первые в Мире покупаем Идеи на любой стадии!Мы ищем тех, кто так же как Мы, может гене рировать идеи постоянно и станет частью нашей Команды!',
            read : 'Read more',
        },
    ],
    Catalog : [
        {
            img : 'catalog-img1.png',
            title : 'We work with Talents, StartUps, Investors, Corporations!',
            text : 'Друзья, Мы поддерживаем развитие Талантов во всём Мире! Сейчас Мы первые в Мире покупаем Идеи на любой стадии! Мы ищем тех, кто так же как Мы, может гене рировать идеи постоянно и станет частью нашей Команды!',
        },
    ],
};
